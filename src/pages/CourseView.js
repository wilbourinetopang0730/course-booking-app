import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CourseView() {

	// The "useParams" hook allows us to retrieve the courseId passed via the URL
	const { courseId } = useParams();

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const enroll = (courseId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${ localStorage.getItem('token') }`
			},

			body: JSON.stringify ({
				courseId: courseId
			})
		})
		.then(res => res.json()).then(data => {
			//console.log(data);

			if (data === true) {
				Swal.fire({
                    icon: 'success',
                    title: 'Successfully enrolled',
                    text: 'You have successfully enrolled for this course.',
                })

				// The navigate hook will allow us to navigate and redirect the user back to the courses page programmatically instead of using a component.
                navigate("/courses");
			} else { 
				Swal.fire({
                    icon: 'error',
                    title: 'Something went wrong',
                    text: 'Please try again.',
                })
			}
		})
	}

	useEffect(() => {
		// console.log(courseId)

		fetch(`${process.env.REACT_APP_API_URL}/courses/${ courseId }`).then(res => res.json())
			.then(data => {
				console.log(data);

				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			})

	}, [courseId]);

	return (
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{ name }</Card.Title>
							
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{ description }</Card.Text>

							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>PhP { price }</Card.Text>

							<Card.Subtitle>Class Schedule</Card.Subtitle>
							<Card.Text>8am - 5pm</Card.Text>

							{
								(user.id) ?
									<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>
								:
									<Link className="btn btn-danger btn-block" to="/login">Log in to enroll</Link>
							}
							{/*<Button variant="primary" onClick={() => enroll(courseId)}>Enroll</Button>*/}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}